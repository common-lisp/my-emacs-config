;;;;__*_ coding:utf-8 __*__
;;;;作者:萍水相逢
;;;;版本:V2.0
;;;;日期:2022
;;;;URL:https://gitee.com/fengzp1230
;;;;说明:Java开发环境配置
;;;;


(message "开始加载java环境配置......")

(use-package meghanada
  :ensure t  ;是否要确保包已经安装
  :defer nil ;是否要延迟加载
  :init      ;初始化参数
  (add-hook 'java-mode-hook
            (lambda ()
              ;; meghanada-mode on
              (meghanada-mode t)
              (flycheck-mode +1)
              (setq c-basic-offset 2)
              ;; use code format
              (add-hook 'before-save-hook 'meghanada-code-beautify-before-save)))
  (cond
   ((eq system-type 'windows-nt)
    (setq meghanada-java-path (expand-file-name "bin/java.exe" (getenv "JAVA_HOME")))
    (setq meghanada-maven-path "mvn.cmd"))
   (t
    (setq meghanada-java-path "java")
    (setq meghanada-maven-path "mvn"))))

(defun lsp-lang-hooks ()
  "Add lsp hooks before save."
  (add-hook 'before-save-hook #'lsp-organize-imports t t)
  (add-hook 'before-save-hook #'lsp-format-buffer t t))

(use-package lsp-mode
  :commands (lsp lsp-deferred lsp-format-buffer lsp-organize-imports)
  :init (setq read-process-output-max (* 1024 1024) ; 1MB, data size read from server, default on 4K
        lsp-auto-guess-root t)
  :hook ((lsp-mode . lsp-enable-which-key-integration)
   (lsp-mode . lsp-lang-hooks)
   (prog-mode . (lambda()(unless (derived-mode-p 'emacs-lisp-mode 'lisp-mode)(lsp-deferred))))))
  
(use-package lsp-ui
  :after lsp-mode
  :commands lsp-ui-mode
  :hook ((lsp-mode . lsp-ui-mode)
         (lsp-ui-mode . lsp-modeline-code-actions-mode))
  :init (setq lsp-ui-doc-include-signature t
              lsp-ui-sideline-ignore-duplicate t
              lsp-modeline-code-actions-segments '(count name)
              lsp-headerline-breadcrumb-enable nil)
  :config
  (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
  (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references))

(use-package dap-mode
  :hook ((lsp-mode . dap-mode)
         (dap-mode . dap-ui-mode)
   (dap-mode . dap-tooltip-mode)
         (python-mode . (lambda() (require 'dap-python)))
         (go-mode . (lambda() (require 'dap-go)))
         (java-mode . (lambda() (require 'dap-java)))))


(message "java环境配置完成")

(provide 'init-development-java)
