;;;;__*_ coding:utf-8 __*__
;;;;作者:萍水相逢
;;;;版本:V2.0
;;;;日期:2022
;;;;URL:https://gitee.com/fengzp1230
;;;;说明:golang开发环境配置
;;;;


(message "开始加载GO环境配置......")

(use-package reformatter)
(use-package format-all :hook (prog-mode . format-all-mode))

(defvar go--tools '("golang.org/x/tools/cmd/goimports"
                    "github.com/go-delve/delve/cmd/dlv"
                    "github.com/josharian/impl"
                    "github.com/cweill/gotests/..."
                    "github.com/fatih/gomodifytags"
                    "github.com/davidrjenni/reftools/cmd/fillstruct")
  "Go tools may needed.")

;; Golang
(use-package go-mode
 ;; :mode "\\.go\\"
  :config
  (use-package go-fill-struct)
  (use-package go-impl)
  (use-package go-gen-test)
  (use-package go-tag)
  :hook
  (go-mode . flycheck-mode)
  (go-mode . lsp-deferred))


(message "GO环境配置完成")

(provide 'init-development-golang)
