;;;;__*_ coding:utf-8 __*__
;;;;作者:萍水相逢
;;;;版本:V2.0
;;;;日期:2022
;;;;URL:https://gitee.com/fengzp1230
;;;;说明:lisp开发环境配置
;;;;


(message "开始加载Lisp环境配置......")

(use-package paredit :hook (emacs-lisp-mode . enable-paredit-mode))


(message "Lisp环境配置完成")

(provide 'init-development-lisp)
