;;;;__*_ coding:utf-8 __*__
;;;;作者:萍水相逢
;;;;版本:V2.0
;;;;日期:2022
;;;;URL:https://gitee.com/fengzp1230
;;;;说明:主题设置
;;;;
(message "开始加载主题配置")

;; Emacs 不再额外对 Theme 的代码安全性确认提示
(setq custom-safe-themes t)

;;主题
(when *is-mac*
  (use-package auto-dark-emacs
    :config
    (setq auto-dark-emacs/dark-theme 'doom-one)
    (setq auto-dark-emacs/light-theme 'doom-solarized-light)
    (setq auto-dark-emacs/polling-interval-seconds 60)
    (auto-dark-emacs/check-and-set-dark-mode)))

(when (or *is-windows* *is-linux*)
  (use-package monokai-theme
    :init (load-theme 'monokai t)))

;;加个好看点的mode-line
; (use-package smart-mode-line
;     :init
;     (setq sml/no-confirm-load-theme t)
;     (setq sml/theme 'respectful)
;     (sml/setup))

(message "主题配置加载完成")
(provide 'init-theme-config)
