;;;;__*_ coding:utf-8 __*__
;;;;作者:萍水相逢
;;;;版本:V2.0
;;;;日期:2022
;;;;URL:https://gitee.com/fengzp1230
;;;;自定义的工具方法
;;;;
;;;;

(message "加载自定义工具方法")

(defconst *user-name* "fengzp")
(defconst *user-email* "wuhen86@gmail.com")

(defconst *is-mac* (eq system-type 'darwin) "Apple macOS platform.")
(defconst *is-linux* (eq system-type 'gnu/linux) "GNU/Linux platform.")
(defconst *is-windows* (memq system-type '(cygwin windows-nt ms-dos)) "Windows / DOS.")

(defconst *is-before-emacs-23* (>= 23 emacs-major-version) "是否是emacs 23或以前的版本")
(defconst *is-after-emacs-27*  (<= 27 emacs-major-version) "是否是emacs 27或以后的版本")
(defconst *is-after-emacs-24*  (<= 24 emacs-major-version) "是否是emacs 24或以后的版本")

;; https://github.com/cabins/.emacs.d/blob/dev/lisp/init-fn.el
(defmacro cabins/timer (&rest body)
  "Measure the time of code BODY running."
  `(let ((time (current-time)))
     ,@body
     (float-time (time-since time))))

(message "自定义工具方法加载完成")
(provide 'init-utils)
