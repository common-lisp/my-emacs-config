;;;; coding:  utf-8
;;;; author： fengzp
;;;; version: 2.0
;;;; data:    2022-09
;;;; desc:    编码配置
;;;;

(message "开始加载编码配置......")


(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-language-environment "UTF-8")
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-buffer-file-coding-system 'utf-8)



(message "编码配置加载完成")
(provide 'init-encoding-config)
