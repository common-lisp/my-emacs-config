;;;;__*_ coding:utf-8 __*__
;;;;作者:萍水相逢
;;;;版本:V2.0
;;;;日期:2022
;;;;URL:https://gitee.com/fengzp1230
;;;;说明:第三方常用包
;;;;
(message "加载第三方常用包")

;;添加额外第三方包
(use-package gnu-elpa-keyring-update)
(use-package diminish)
(use-package delight)

;;重启emacs
(use-package restart-emacs)

;;自动识别文件编码
(use-package unicad
  :ensure t
  :init
  (require 'unicad))

;;使用maxframe包，使启动时窗口最大化
;; (use-package maxframe
;;   :ensure t
;;   :init
;;   (add-hook 'window-setup-hook 'maximize-frame t))

;;上下移动行/块
(use-package drag-stuff
  :bind (("<M-up>". drag-stuff-up)
         ("<M-down>" . drag-stuff-down)))

;;当你按下一个按键时，它会提示你下一个按键的含义
(use-package which-key
  :defer nil
  :hook (after-init . which-key-mode)
  :config
  (which-key-add-key-based-replacements
    "C-c !" "flycheck"
    "C-c @" "hideshow"
    "C-c i" "ispell"
    "C-c t" "hl-todo"
    "C-x a" "abbrev"
    "C-x n" "narrow"
    "C-x t" "tab")
  :custom
  (which-key-idle-delay 0.5)
  (which-key-add-column-padding 1))

;;大名顶顶的自动补全插件
;; (use-package company
;;   :config
;;   (setq company-dabbrev-code-everywhere t
;;         company-dabbrev-code-modes t
;;         company-dabbrev-code-other-buffers 'all
;;         company-dabbrev-downcase nil
;;         company-dabbrev-ignore-case t
;;         company-dabbrev-other-buffers 'all
;;         company-require-match nil
;;         company-minimum-prefix-length 2                  ; 只需敲2个字母就开始进行自动补全
;;         company-show-numbers t                            ;; 给选项编号 (按快捷键 M-1、M-2 等等来进行选择).
;;         company-selection-wrap-around t
;;         company-transformers '(company-sort-by-occurrence) ; 根据选择的频率进行排序，读者如果不喜欢可以去掉
;;         company-idle-delay 0
;;         company-echo-delay 0
;;         company-tooltip-limit 20
;;         company-tooltip-offset-display 'scrollbar
;;         company-tooltip-align-annotations t
;;         company-begin-commands '(self-insert-command))
;;   (push '(company-semantic :with company-yasnippet) company-backends)
;;   :hook ((after-init . global-company-mode)))


;;语法检查  可以使用自带的flymake 在init-buildin配置文件中已启用
;;(use-package flycheck
;;  :hook (after-init . global-flycheck-mode)
;;        (prog-mode . flycheck-mode))

;;彩虹括号
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;;格式化代码
(use-package format-all
  :diminish
  :hook (prog-mode . format-all-ensure-formatter)
  :bind ("C-c f" . #'format-all-buffer))

;; Settings for yasnippet
(use-package yasnippet
  :defer 2
  :diminish yas-minor-mode
  :init (yas-global-mode)
  :bind (("C-o" . yas-expand))
  :config (use-package yasnippet-snippets :after yasnippet))


;; (use-package ivy-posframe
;;   :init
;;   (setq ivy-posframe-display-functions-alist
;;     '((swiper . ivy-posframe-display-at-frame-center)
;;       (complete-symbol . ivy-posframe-display-at-point)
;;       (counsel-M-x . ivy-posframe-display-at-frame-center)
;;       (counsel-find-file . ivy-posframe-display-at-frame-center)
;;       (ivy-switch-buffer . ivy-posframe-display-at-frame-center)
;;       (t . ivy-posframe-display-at-frame-center)))


;; All the icons
;; If the icons are not displayed correctly although all-the-icons fonts are installed correctly
;; please install the non-free font Symbola. This issue usually occurs on Windows.
;; [Refs] https://github.com/seagle0128/doom-modeline
(use-package all-the-icons
  :when (display-graphic-p))

;;加载第三方字体配置
;; (use-package cnfonts
;;   :ensure t
;;   ;;:after all-the-icons
;;   :hook (cnfonts-set-font-finish
;;          . (lambda (fontsizes-list)
;;              (set-fontset-font t 'unicode (font-spec :family "all-the-icons") nil 'append)
;;              (set-fontset-font t 'unicode (font-spec :family "file-icons") nil 'append)
;;              (set-fontset-font t 'unicode (font-spec :family "Material Icons") nil 'append)
;;              (set-fontset-font t 'unicode (font-spec :family "github-octicons") nil 'append)
;;              (set-fontset-font t 'unicode (font-spec :family "FontAwesome") nil 'append)
;;              (set-fontset-font t 'unicode (font-spec :family "Weather Icons") nil 'append)))
;;   :config
;;   (cnfonts-enable))

;;平滑滚动
(use-package good-scroll
  :ensure t
  :init (good-scroll-mode))

;; ctrlf, good isearch alternative
(use-package ctrlf
  :hook (after-init . ctrlf-mode))

;;输入的历史命令放在最前面
(use-package amx
  :ensure t
  :init (amx-mode))


;;有道翻译
(use-package youdao-dictionary
  :config
  (setq youdao-dictionary-search-history-file (concat user-emacs-directory ".youdao")) ;;设置临时目录
  (setq url-automatic-caching t) ;;启用缓存
  (setq youdao-dictionary-use-chinese-word-segmentation t) ;;支持中文分词
  :bind
  (("C-c y" . 'youdao-dictionary-search-at-point)))

;;快速运行
(use-package quickrun
  :ensure t
  :bind ("C-c x" . quickrun)
  :custom
  (quickrun-focus-p nil)
  (quickrun-input-file-extension ".qr"))

;;辅助线
(use-package highlight-indent-guides
  :custom
  (highlight-indent-guides-method 'character)
  (highlight-indent-guides-responsive 'top)
  (highlight-indent-guides-auto-enabled nil)
  ;;  :custom-face  ; NOTE: The character does not work with "RobotoMono Nerd Font"
  ;;  (highlight-indent-guides-character-face ((t (:family "Source Code Pro" :foreground ,fk/light-color7))))
  ;;  (highlight-indent-guides-top-character-face ((t (:family "Source Code Pro" :foreground ,fk/light-color5))))
  :hook
  (prog-mode . highlight-indent-guides-mode))

;;注释
(use-package newcomment
  :ensure nil
  :bind ([remap comment-dwim] . comment-or-uncomment)
  :config
  (defun comment-or-uncomment ()
    "Comment or uncomment the current line or region.
If the region is active and `transient-mark-mode' is on, call
`comment-or-uncomment-region'.
Else, if the current line is empty, insert a comment and indent
it.
Else, call `comment-or-uncomment-region' on the current line."
    (interactive)
    (if (region-active-p)
        (comment-or-uncomment-region (region-beginning) (region-end))
      (if (save-excursion
            (beginning-of-line)
            (looking-at "\\s-*$"))
          (comment-dwim nil)
        (comment-or-uncomment-region (line-beginning-position) (line-end-position)))))
  :custom
  ;; `auto-fill' inside comments.
  ;;
  ;; The quoted text in `message-mode' are identified as comments, so only
  ;; quoted text can be `auto-fill'ed.
  (comment-auto-fill-only-comments t))


(message "第三方常用包加载完成")
(provide 'init-common-package)
