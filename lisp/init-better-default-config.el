;;;;__*_ coding:utf-8 __*__
;;;;作者:萍水相逢
;;;;版本:V2.0
;;;;日期:2022
;;;;URL:https://gitee.com/fengzp1230
;;;;说明:个人认为比较好用的默认设置
;;;;
;;;;
(message "加载默认设置")

(require 'init-utils)

;;设置有用的个人信息.这在许多地方有用。  在init-utils中定义常量
(setq user-full-name *user-name*)
(setq user-mail-address *user-email*)

;;设置标题栏
(setq frame-title-format
      (list (format "%s - Emacs@%s %%S: %%j " *user-name* (system-name))
            '(buffer-file-name "%f" (dired-directory dired-directory "%b"))))

;;(electric-pair-mode t)                       ; 自动补全括号
(add-hook 'after-init-hook 'electric-indent-mode)
(add-hook 'prog-mode-hook 'electric-pair-mode)
(add-hook 'prog-mode-hook 'electric-layout-mode)
(add-hook 'prog-mode-hook #'show-paren-mode) ; 编程模式下，光标在括号上时高亮另一个括号
(column-number-mode t)                       ; 在 Mode line 上显示列号
(unless *is-linux*
  (global-linum-mode 0))                     ;;显示行号
(global-auto-revert-mode t)                  ; 当另一程序修改了文件时，让 Emacs 及时刷新 Buffer
(delete-selection-mode t)                    ; 选中文本后输入文本会替换文本（更符合我们习惯了的其它编辑器的逻辑）
(setq inhibit-startup-message t)             ; 关闭启动 Emacs 时的欢迎界面
(setq make-backup-files nil)                 ; 关闭文件自动备份
(setq-default make-backup-files nil)         ;不要生成临时文件
(setq backup-inhibied t)                     ;不产生备份
(setq auto-save-default nil)                 ;不生成名为#filename#的临时文件
(add-hook 'prog-mode-hook #'hs-minor-mode)   ; 编程模式下，可以折叠代码块
(global-display-line-numbers-mode 1)         ; 在 Window 显示行号
(tool-bar-mode -1)                           ; 关闭 Tool bar
;(menu-bar-mode -1)                          ;;关闭菜单栏
(scroll-bar-mode -1)  		             ;;关闭滚动栏
(when (display-graphic-p) (toggle-scroll-bar -1)) ; 图形界面时关闭滚动条
(setq default-fill-column 120)                ;;把 fill-column 设为 120. 这样的文字更好读。
(global-hl-line-mode 1)                       ;;高亮显示光标所在行

(blink-cursor-mode -1)

(setq tab-width 4)                            ;;设置tab键为4个空格
(setq-default indent-tabs-mode nil)           ;;使用空格代替TAB use C-q TAB to input the TAB char

(setq select-enable-clipboard t)              ;;支持 emacs 和外部程序的粘贴

(setq ring-bell-function 'ignore)            ;;关闭警告
(setq visible-bell t)                        ;;取消警告声音
(fset 'yes-or-no-p 'y-or-n-p)                ;不要总是没完没了的问yes or no, 为什么不能用 y/n
(icomplete-mode 1)                           ;用M-x执行某个命令的时候，在输入的同时给出可选的命令名提示
(setq initial-scratch-message "")            ;初始化 scratch buffer 为空(不显示里面的注释)

(setq-default cursor-type 'bar)               ;;设置光标为单竖线
;(set-cursor-color "green")                   ;;设置光标颜色
(setq inhibit-splash-screen 1)		      ;;取消启动帮助
(global-font-lock-mode t)	              ;;语法高亮
(display-time)				      ;;显示时间

(setq-default kill-whole-line t)	      ;;C-k 删除该行

;; 设置时间戳，标识出最后一次保存文件的时间。
(setq time-stamp-active t)
(setq time-stamp-warn-inactive t)
(setq time-stamp-format "%:y-%02m-%02d %3a %02H:%02M:%02S fengzp")


(setq default-directory user-emacs-directory) ;;设置打开的默认目录为~/.emacs.d

;;开启最大化 也可使用maxframe包，在init-common-package中
(setq initial-frame-alist (quote ((fullscreen . maximized))))


;;开启tab模式
;;(tab-bar-mode)
;;(tab-line-mode)


;;高亮显示选择区域
(setq-default transient-mark-mod t)
(transient-mark-mode t)

;按照windowz用户的习惯使用 `C-x C-c C-v'
;(setq cua-mode t)

;按照windows用户的习惯,使用 'Ctrl Alt Shift + 方向键移动和选择文本'
;(setq pc-selection-mode t)




;;退出时询问
(setq kill-emacs-query-functions
      (lambda ()(y-or-n-p "Do you really want to quit? ")))


(defun kill-shell  ()
   (set-process-sentinel (get-buffer-process (current-buffer))
                            #'kill-shell-buffer-on-exit))
(defun kill-shell-buffer-on-exit (process state)
  (kill-buffer (current-buffer)))
 ;;退出shell时删除shell buffer
(add-hook 'shell-mode-hook 'kill-shell)



;;编译成功后自动关闭*compilation* 函数
(defun kill-buffer-when-compile-success (process)
  "Close current buffer when `shell-command' exit."
	(set-process-sentinel process
		(lambda (proc change)
			(when (string-match "finished" change)
				(delete-windows-on (process-buffer proc))))))

;; 编译成功后自动关闭*compilation* buffer
(add-hook 'compilation-start-hook 'kill-buffer-when-compile-success)

;;设置垃圾回收阈值，加速启动速度。
(setq gc-cons-threshold most-positive-fixnum)


;;退出的时候删掉server文件(注意:server文件夹需要用户权限)
(add-hook 'kill-emacs-hook'
 		  (lambda()
 			(if (file-exists-p "~/.emacs.d/server/server")
 				(delete-file "~/.emacs.d/server/server"))))
;;启动GNU Server
(server-start)
(server-mode 1)

(message "默认设置加载完成")
(provide 'init-better-default-config)
