;;;;
;;;;emacs内置功能配置

(message "开始加载内置插件配置")


;;缩写快速替换,类似于  C-x a i g 来定义
;; (use-package abbrev
;;   :ensure nil
;;   :init (setq-default abbrev-mode t)
;;   ;;重新定义缩写文件位置  默认:~/.abbrev_defs
;;   (setq abbrev-file-name "~/.emacs.d/abbrev_defs"))

;; windmove
(use-package windmove
  :ensure nil
  :bind (("C-c <left>" . windmove-left)
         ("C-c <right>" . windmove-right)
         ("C-c <up>" . windmove-up)
         ("C-c <down>" . windmove-down)))


;; Flymake 语法检查  是emacs内置的插件，类似于flycheck
;;(add-hook 'prog-mode-hook 'flymake-mode)
;; (use-package flymake
;;   :ensure nil
;;   :diminish (flymake " Flym.")
;;   :hook (prog-mode . flymake-mode)
;;   :bind (("M-n" . flymake-goto-next-error)
;;          ("M-p" . flymake-goto-prev-error)))

;; HideShow Minor Mode
;; 用于展开或折叠代码块
;; (use-package hideshow
;;   :init (add-hook 'hs-minor-mode-hook (lambda () (diminish 'hs-minor-mode)))
;;   :hook (prog-mode . hs-minor-mode))

;; Delete Behavior
;;(add-hook 'before-save-hook #'delete-trailing-whitespace)
;;(add-hook 'after-init-hook 'delete-selection-mode)

;; ibuffer
(use-package ibuffer
  :init (defalias 'list-buffers 'ibuffer))

;; Ido ( instead of ivy & counsel & swiper)
(use-package ido
  :defer nil
  :init (setq ido-enable-flex-matching t
              ido-auto-merge-work-directories-length -1
              isearch-lazy-count t
              lazy-count-prefix-format "%s/%s: "
              ido-everywhere t
              ido-use-filename-at-point t
              completion-ignored-extensions '(".o" ".elc" "~" ".bin" ".bak" ".obj" ".map" ".a" ".ln" ".class"))
  (ido-mode t)
  (fido-mode t))

;; Line Number
;; this package introduced in Emacs 26, so only enabled when 26+
(use-package display-line-numbers
  :if (> emacs-major-version 26)
  :hook (prog-mode . display-line-numbers-mode))

;; Org Mode
(setq org-hide-leading-stars t
      org-startup-indented t)

;; 高亮匹配的括号
(use-package paren
  :ensure nil
  :config (setq-default show-paren-style 'mixed
                        show-paren-when-point-inside-paren t
                        show-paren-when-point-in-periphery t)
  :hook (prog-mode . show-paren-mode))

;;打开最近文件列表
(use-package recentf
  :init
  (setq recentf-max-menu-item 20)
  :defer 1
  :hook (after-init . recentf-mode)
  :bind
  (("C-x C-r" . 'recentf-open-files))
  :custom
  (recentf-max-saved-items 30)
  (recentf-auto-cleanup 'never)
  (recentf-exclude '(;; Folders on MacOS start
                     "^/private/tmp/"
                     "^/var/folders/"
                     ;; Folders on MacOS end
                     "^/tmp/"
                     "/ssh\\(x\\)?:"
                     "/su\\(do\\)?:"
                     "^/usr/include/"
                     "~\/.emacs.d\/elpa\/"
                     "/TAGS\\'"
                     "COMMIT_EDITMSG\\'")))

;; 保存访问过文件的光标位置
(use-package saveplace
  :hook (after-init . save-place-mode))


(message "内置插件配置加载完成")
(provide 'init-buildin)
