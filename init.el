;;;;__*_ coding:utf-8 __*__
;;;;作者:萍水相逢
;;;;版本:V2.0
;;;;日期:2022
;;;;URL:https://gitee.com/fengzp1230
;;;;说明:emacs初始加载文件
;;;;


(message  (concat "我的emacs版本:" emacs-version))
(message (concat "Home目录" (getenv "HOME")))
(message (concat "emacs目录:" user-emacs-directory ))

;;配置文件所在目录
(defconst base-config-dir (file-name-directory load-file-name))
;;设置自定义文件
(setq custom-file (expand-file-name "custom.el" base-config-dir))

;;把自定义的配置加入load-path
;;(add-to-list 'load-path "~/.emacs.d/lisp/")
(add-to-list 'load-path (expand-file-name (concat user-emacs-directory "lisp/")))


;; Settings for Emacs 28+
(when (and (fboundp 'native-comp-available-p)
     (native-comp-available-p))
  (setq native-comp-async-report-warnings-errors nil
        comp-deferred-compilation t
        package-native-compile t)
  (add-to-list 'native-comp-eln-load-path (expand-file-name "eln-cache" user-emacs-directory)))

;;加载自定义常量、函数、宏
(require 'init-utils)
;;加载自定义包管理
(require 'init-melpa)
;;加载默认配置
(require 'init-better-default-config)
;;加载emacs内置包设置
(require 'init-buildin)
;;加载主题配置
(require 'init-theme-config)
;;加载modeline配置
(require 'init-modeline-config)
;;加载第三方常用包
(require 'init-common-package)

;;字体配置 使用cnfonts三方包
;;(require 'init-font-config)
;;编码配置
(require 'init-encoding-config)
;;ivy专项配置
;;(require 'init-ivy-config)
;;侧边栏配置
;;(Require 'init-sidebar)
;;版本控制配置
(require 'init-cvs-config)
;;java开发环境配置
;;(require 'init-development-java)
;;go开发环境配置
;;(require 'init-development-golang)
;;web开发环境配置
;;(require 'init-development-web)
;;lisp开发环境配置
;;(require 'init-development-lisp)

;;VIM按键配置
;;(require 'init-evil-config)
