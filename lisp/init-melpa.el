;;;;__*_ coding:utf-8 __*__
;;;;作者:萍水相逢
;;;;版本:V2.0
;;;;日期:2022
;;;;URL:https://gitee.com/fengzp1230
;;;;说明:包管理器配置
;;;;

(message "开始加载包管理配置")
;;初始化包源
(require 'package)
(setq package-check-signature nil
      load-prefer-newer t)
(setq package-archives '(("gnu" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/gnu/")
                         ("melpa" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/melpa/")
                         ("org" . "http://mirrors.tuna.tsinghua.edu.cn/elpa/org/")))
;; (setq package-archives '(("gnu"   . "http://mirrors.cloud.tencent.com/elpa/gnu/")
;;                          ("melpa" . "http://mirrors.cloud.tencent.com/elpa/melpa/")))

(unless (bound-and-true-p package--initialized) ;; To avoid warnings on 27
  (package-initialize))

(unless package-archive-contents
  (package-refresh-contents))

;; settings for use-package package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t ;不用每个包都手动添加:ensure t关键字
      use-package-always-defer nil ;默认都是延迟加载，不用每个包都手动添加:defer t
      use-package-always-demand nil
      use-package-expand-minimally t
      use-package-verbose t)


;; 常用的格式
;; (use-package smooth-scrolling
;;    :ensure t ;是否一定要确保已安装
;;    :defer nil ;是否要延迟加载
;;    :init (setq smooth-scrolling-margin 2) ;初始化参数
;;    :config (smooth-scrolling-mode t) ;基本配置参数
;;    :bind ;快捷键的绑定
;;    :hook) ;hook模式的绑定

(use-package auto-package-update
  :init (setq auto-package-update-delete-old-versions t
        auto-package-update-hide-results t))

;;通过 benchmark-init 包进行启动耗时的测量
;;M-x benchmark-init/show-durations-tree 或者 M-x benchmark-init/show-durations-tabulated
(use-package benchmark-init
  :init (benchmark-init/activate)
  :hook (after-init . benchmark-init/deactivate))


(message "包管理配置加载完成")
(provide 'init-melpa)
